Cadernos Python

Jupyter Notebooks com exemplos e explicações sobre fundamentos da linguagem Python.

https://mybinder.org/v2/git/https%3A%2F%2Fcodeberg.org%2Fgrz%2Fcdpython01.git/main

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fcodeberg.org%2Fgrz%2Fcdpython01.git/main)

...